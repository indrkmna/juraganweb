      <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
          <div class="col-12 col-md">
            <img class="mb-2" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
            <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
          </div>
          <div class="col-6 col-md">
            <h5>Bantuan</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Kebijakan Privasi</a></li>         
              <li><a class="text-muted" href="#">Syarat & Ketentuan</a></li>
              <li><a class="text-muted" href="#">Cara Pemesanan</a></li>
              <li><a class="text-muted" href="#">Cara Transfer</a></li>           
              <li><a class="text-muted" href="#">Komplain Pesanan</a></li>            
              <li><a class="text-muted" href="#">FAQ (Tanya & Jawab)</a></li>                             
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Sumber Daya</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="https://codeigniter.com/" target="_blank">CodeIgniter</a></li>
              <li><a class="text-muted" href="#">Bootstrap</a></li>
              <li><a class="text-muted" href="#">MySQL</a></li>
              <li><a class="text-muted" href="#">jQuery</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Informasi</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Hubungi Kami</a></li>
              <li><a class="text-muted" href="#">Alamat</a></li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  </body>
</html>
