<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h3>Silahkan Pilih Paket</h3>
    </div>

    <div class="container">
      <div class="card-deck mb-3 text-center">
        <?php foreach($packages as $list){ ?>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal"><?php echo $list->name;?></h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title"><?php echo $list->price;?></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <?php echo $list->description;?>
            </ul>
            <a href="<?php echo base_url('register/package/'.$list->id);?>" class="btn btn-lg btn-block btn-primary">Daftar Sekarang</a>
          </div>
        </div>
        <?php } ?>
      </div>
