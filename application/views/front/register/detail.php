    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h3>Rincian Paket <?php echo $package->name;?></h3>
    </div>
    <?php if(!empty($this->session->flashdata('message_error'))){?>
          <div class="alert alert-danger">
          <?php   
             print_r($this->session->flashdata('message_error'));
          ?>
          </div>
          <?php }?> 

    <div class="container">
      <div class="card-deck mb-3">
        <div class="card mb-6 box-shadow text-center">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal"><?php echo $package->name;?></h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title"><?php echo $package->price;?></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <?php echo $package->description;?>
            </ul> 
          </div>
        </div>
        <div class="col-md-6">
          <form action="<?php echo base_url('register/package/'.$package->id);?>" method="POST" id="form">
            <input type="hidden" name="role_id" value="2">            
            <input type="hidden" name="package_id" value="<?php echo $package->id;?>">            
            <label class="mb-3"><b>Nama Depan</b></label>
            <input class="form-control mb-3" type="text" name="first_name">
            <label class="mb-3"><b>Nama Belakang</b></label>
            <input class="form-control mb-3" type="text" name="last_name">
            <label class="mb-3"><b>Email</b></label>            
            <input class="form-control mb-3" type="email" name="email">
            <label class="mb-3"><b>Telepon</b></label>            
            <input class="form-control mb-3" type="number" name="phone">            
            <label class="mb-3"><b>Password</b></label>            
            <input class="form-control mb-3" type="password" name="password">
            <br>
            <button class="btn btn-primary btn-lg" type="submit">Daftar</button>
          </form>
        </div>        
      </div>
