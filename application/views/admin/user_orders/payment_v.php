<section class="content">
  <div class="full-width padding">
  <div class="padding-top">
    <div class="row"> 
      <div class="col-md-6"> 
        <div class="box box-default color-palette-box">
    
    <div class="box-body">
      <div class="row">
        <div class="col-md-12"> 
            <div class="table-responsive">
            <table class="table table-striped display nowrap" id="table_payment"> 
              <thead>
                <th>No</th> 
                <th>Owner</th> 
                <th>Cash </th> 
                <th>Tanggal Pembayaran</th> 
                <th>Status</th> 
              </thead>        
            </table>
          </div>
        </div>
      </div>
    </div>
  </div> 
      </div>
      <div class="col-md-6"> 
        <div class="box box-primary">
          <form class="form-horizontal" id="form_konfirmasi" method="POST" action="" enctype="multipart/form-data">
          <div class="box-body">
              <?php if(!empty($this->session->flashdata('message_error'))){?>
                <div class="alert alert-danger">
                <?php   
                   print_r($this->session->flashdata('message_error'));
                ?>
                </div>
                <?php }?> 

                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 control-label">Owner</label> 
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="owner" placeholder="Owner" name="owner" >
                  </div>
                </div>

                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 control-label">Cash</label> 
                  <div class="col-sm-9">
                    <input type="text" class="form-control number" id="cash" placeholder="Cash" name="cash" >
                  </div>
                </div>

                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Pembayaran</label> 
                  <div class="col-sm-9">
                    <input type="date" class="form-control number" id="created" placeholder="Tanggal Pembayaran" name="created" >
                  </div>
                </div>

                <div class="form-group row">
                <label for="inputEmail3" class="col-sm-3 control-label">Bukti Pembayaran</label> 
                  <div class="col-sm-9">
                    <input type="file" class="form-control" id="image" name="image">
                    
                  </div> 
                </div>  
            </div> 
            <div class="box-footer pad-15 full-width bg-softgrey border-top bot-rounded text-right">
              <button type="submit" class="btn btn-primary mleft-15" id="save-btn">Simpan</button>
              <a href="<?php echo base_url();?>user" class="btn btn-default">Batal</a>
            </div>
          </form>
        </div>  
      </div> 
    </div>
  </div>
</div>
</section>

 <script data-main="<?php echo base_url()?>assets/js/main/main-user-orders" src="<?php echo base_url()?>assets/js/require.js"></script>