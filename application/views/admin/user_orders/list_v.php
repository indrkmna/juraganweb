 <section class="content-header">
  <h1>
    <?php echo ucwords(str_replace("_"," ",$this->uri->segment(1)))?>
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?php echo ucwords(str_replace("_"," ",$this->uri->segment(1)))?></li>
  </ol>
</section>

<section class="content">
  <div class="box box-bottom">
    <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-tag"></i> Pencarian <?php echo ucwords(str_replace("_"," ",$this->uri->segment(1)))?></h3>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">  
       <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 control-label">Nama User</label> 
            <div class="col-sm-4">
              <input type="text" class="form-control" id="name" placeholder="Nama User" name="name">
            </div>
           <label for="inputPassword3" class="col-sm-2 control-label">Nama Paket</label> 
            <div class="col-sm-4">
             <input type="text" class="form-control" id="paket" placeholder="Nama Paket" name="paket">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 control-label">Status</label> 
            <div class="col-sm-4">
              <select class="form-control" name="status" id="status">
                <option value="" selected>Semua Status</option>
                <option value="0">Lunas</option>
                <option value="1">Belum Lunas</option>
              </select>
            </div>
          </div> 
          <div class="form-group row">
            <div class="col-sm-12 text-right"> 
              <button class="btn btn-sm btn-danger" id="reset">Hapus</button>
              <button class="btn btn-sm btn-primary" id="search">Cari</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box box-default color-palette-box">
    <div class="box-header with-border">
       <div class="full-width datatableButton text-right">
          <a href="<?php echo base_url()?>user_orders/create" class="btn btn-sm btn-primary pull-right"><i class='fa fa-plus'></i> Tambah Order</a>
        </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12"> 
            <div class="table-responsive">
            <?php if(!empty($this->session->flashdata('message'))){?>
            <div class="alert alert-info">
            <?php   
               print_r($this->session->flashdata('message'));
            ?>
            </div>
            <?php }?> 
             <?php if(!empty($this->session->flashdata('message_error'))){?>
            <div class="alert alert-danger">
            <?php   
               print_r($this->session->flashdata('message_error'));
            ?>
            </div>
            <?php }?> 
            <table class="table table-striped display nowrap" id="table"> 
              <thead>
                <th>No Urut</th> 
                <th>Nama User</th> 
                <th>Paket </th> 
                <th>Harga Paket</th> 
                <th>Status</th> 
                <th>Aksi</th> 
              </thead>        
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script 
  data-main="<?php echo base_url()?>assets/js/main/main-user-orders" 
  src="<?php echo base_url()?>assets/js/require.js">  
</script>