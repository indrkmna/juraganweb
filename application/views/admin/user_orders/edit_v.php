<section class="content">
<div class="full-width padding">
  <div class="box box-default color-palette-box">
    <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-tag"></i> Tambah Pengguna Baru</h3>
    </div>
      <form class="form-horizontal" id="form" method="POST" action="" enctype="multipart/form-data">
      <input type="hidden" name="id" id="id" value="<?php echo $id?>">
        <div class="box-body">
          <?php if(!empty($this->session->flashdata('message_error'))){?>
          <div class="alert alert-danger">
          <?php   
             print_r($this->session->flashdata('message_error'));
          ?>
          </div>
          <?php }?>  
           <div class="form-group row">
            <label for="inputEmail3" class="col-sm-3 control-label">User</label> 
            <div class="col-sm-9">
              <select name="user_id" id="user_id" class="form-control">
                <option value="">Semua User <?php echo $user_id?></option>
                <?php foreach($user as $data){?>
                  <option value="<?php echo $data->id ?>" <?php echo $data->id == $user_id ? 'selected' : '' ?>><?php echo $data->first_name." ".$data->last_name?></option>
                <?php }?>
              </select>
            </div>
          </div> 
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-3 control-label">Paket Harga</label> 
              <div class="col-sm-9">
                <select name="package_id" id="package_id" class="form-control">
                  <option value="">Semua Paket Harga</option>
                  <?php foreach($packages as $data){?>
                    <option value="<?php echo $data->id ?>" <?php echo $data->id == $package_id ? 'selected' : '' ?>><?php echo $data->name."( Rp. ".number_format($data->price).")"?></option>
                  <?php }?>
              </select>
              </div> 
            </div>
        </div> 
        <div class="box-footer pad-15 full-width bg-softgrey border-top bot-rounded text-right">
          <button type="submit" class="btn btn-primary mleft-15" id="save-btn">Simpan</button>
          <a href="<?php echo base_url();?>user_orders" class="btn btn-default">Batal</a>
        </div>
      </form>
    </div>  
</section>
 
<script data-main="<?php echo base_url()?>assets/js/main/main-user-orders" src="<?php echo base_url()?>assets/js/require.js"></script>