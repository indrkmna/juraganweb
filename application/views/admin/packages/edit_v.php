<section class="content">
<div class="full-width padding">
  <div class="box box-default color-palette-box">
    <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-tag"></i> Tambah Paket Baru</h3>
    </div>
      <form class="form-horizontal" id="form" method="POST" action="" enctype="multipart/form-data">
        <div class="box-body">
          
          <?php if(!empty($this->session->flashdata('message_error'))){?>
          <div class="alert alert-danger">
          <?php   
             print_r($this->session->flashdata('message_error'));
          ?>
          </div>
          <?php }?> 

           <div class="form-group row">
            <label for="inputEmail3" class="col-sm-3 control-label">Nama Paket</label> 
            <div class="col-sm-9">
              <input type="name" class="form-control" id="name" placeholder="Nama Paket" name="name" value=" <?php echo $name; ?>">
            </div>
          </div> 

          <div class="form-group row">
            <label  class="col-sm-3 control-label">Harga Paket</label> 
            <div class="col-sm-9">
             <input type="text" class="form-control number" maxlength="13" id="price" placeholder="Harga Paket" name="price" value="<?php echo $price; ?>">
            </div>
          </div>
          <hr> 
          <div class="form-group row">
            <label class="col-sm-3 control-label">Deskripsi</label> 
            <div class="col-sm-9">
             <textarea class="form-control" name="description"><?php echo $description; ?></textarea>
            </div>
          </div> 
             
        </div> 
        <div class="box-footer pad-15 full-width bg-softgrey border-top bot-rounded text-right">
          <button type="submit" class="btn btn-primary mleft-15" id="save-btn">Simpan</button>
          <a href="<?php echo base_url();?>packages" class="btn btn-default">Batal</a>
        </div>
      </form>
    </div>  
</section>
 
<script data-main="<?php echo base_url()?>assets/js/main/main-packages" src="<?php echo base_url()?>assets/js/require.js"></script>