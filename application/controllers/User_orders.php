<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'core/Admin_Controller.php';
class User_orders extends Admin_Controller {
 	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_orders_model');
		$this->load->model('billing_model');
		$this->load->model('payment_model');
		$this->load->model('packages_model');
		$this->load->model('user_model');
		$this->load->model('ion_auth_model');
	}
	public function index()
	{
		$this->load->helper('url');
		if($this->data['is_can_read']){ 
			$this->data['content'] = 'admin/user_orders/list_v'; 	
		}else{
			$this->data['content'] = 'errors/html/restrict'; 
		}
		
		$this->load->view('admin/layouts/page',$this->data);  
	}


	public function create()
	{ 
		$this->form_validation->set_rules('package_id',"Paket Harga", 'trim|required');  
		if ($this->form_validation->run() === TRUE)
		{
			$data = array(
				'user_id' => $this->input->post('user_id'),
				'package_id' => $this->input->post('package_id'),
				'is_active' => 0
			); 
			$insert = $this->billing_model->insert($data);
			if ($insert)
			{ 
				$this->session->set_flashdata('message', "User Order Berhasil Disimpan");
				redirect("user_orders");
			}
			else
			{
				$this->session->set_flashdata('message_error',"User Order Gagal Disimpan");
				redirect("user_orders");
			}
		}else{   
		 	$this->data['user'] = $this->user_model->getAllById();
		 	$this->data['packages'] = $this->packages_model->getAllById();
			$this->data['content'] = 'admin/user_orders/create_v'; 
			$this->load->view('admin/layouts/page',$this->data); 
		}
	} 

	public function edit($id)
	{ 
		$this->form_validation->set_rules('user_id',"User", 'trim|required');  
		$this->form_validation->set_rules('package_id',"Paket", 'trim|required');  
		   
		if ($this->form_validation->run() === TRUE)
		{
			$data = array(
				'user_id' 		=> $this->input->post('user_id'),
				'package_id' 	=> $this->input->post('package_id'),
				
				
			);
		    
			$update = $this->billing_model->update($data, array('billing.id' => $id));
			if ($update)
			{ 
				$this->session->set_flashdata('message', "User Order Berhasil Diubah");
				redirect("user_orders","refresh");
			}else{
				$this->session->set_flashdata('message_error', "User Order Gagal Diubah");
				redirect("user_orders","refresh");
			}
		} 
		else
		{
			if(!empty($_POST)){ 

				$id = $this->input->post('id'); 
				$this->session->set_flashdata('message_error',validation_errors());
				return redirect("user_orders/edit/".$id);	

			}else{

				$this->data['id']= $id;
				$data = $this->billing_model->getOneBy(array("billing.id"=>$this->data['id'])); 
			  
		 		$this->data['user']	  	= $this->user_model->getAllById();
		 		$this->data['packages']	  	= $this->packages_model->getAllById();

				$this->data['user_id'] 		= (!empty($data))?$data->user_id:"";
				$this->data['package_id'] 	= (!empty($data))?$data->package_id:"";

				$this->data['content']		= 'admin/user_orders/edit_v'; 
				$this->load->view('admin/layouts/page',$this->data); 
			}  
		}    
		
	} 

	public function dataList()
	{
		$columns = array( 
            0 =>'billing.id',  
      		1 =>'users.first_name', 
            2 =>'package_name',
            3 =>'price',
            4 => 'is_active', 
            6 => 'action'
        ); 
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
  		$search = array();
  		$limit = 0;
  		$start = 0;
        $totalData = $this->billing_model->getCountAllBy($limit,$start,$search,$order,$dir); 

        $searchColumn = $this->input->post('columns');
        $isSearchColumn = false;
        
        if(!empty($searchColumn[3]['search']['value'])){
        	$value = $searchColumn[3]['search']['value'];
        	$isSearchColumn = true;
         	$search['users.first_name'] = $value;
        }  

      	if(!empty($searchColumn[4]['search']['value'])){
        	$value = $searchColumn[4]['search']['value'];
        	$isSearchColumn = true;
         	$search['packages.name'] = $value;
		}
		
		if(!empty($searchColumn[5]['search']['value'])){
			$search_value = $searchColumn[5]['search']['value'];
			$isSearchColumn = true;
			$search = array( 
				"user_orders.status"=>$search_value
			); 
		}

    	if($isSearchColumn){
			$totalFiltered = $this->billing_model->getCountAllBy($limit,$start,$search,$order,$dir); 
        }else{
        	$totalFiltered = $totalData;
        } 
       
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
		$datas = $this->billing_model->getAllBy($limit,$start,$search,$order,$dir);
     	
        $new_data = array();
        if(!empty($datas))
        { 
            foreach ($datas as $key=>$data)
            {  

            	$edit_url = "";
            	$konfirmasi_url = "";
            	$payment_url = "";
     			$delete_url = "";
     		
            	if($this->data['is_can_edit']){
            		$edit_url = "<a href='".base_url()."user_orders/edit/".$data->id."' class='btn btn-primary btn-sm white'><i class='fa fa-pencil'></i> Ubah</a>";

            		if($data->is_active == 0){
	        			$konfirmasi_url = "<button 
	        				url='".base_url()."user_orders/konfirmasi/".$data->id."/".$data->is_active."'
	        				class='btn btn-danger btn-sm white delete' >Konfirmasi
	        				</button>";
	        		}else{
	        			$konfirmasi_url = "<button 
	        				url='".base_url()."user_orders/konfirmasi/".$data->id."/".$data->is_active."'
	        				class='btn btn-danger  btn-sm white delete' 
	        				 >NonKonfirmasi
	        				</button>";
	        		} 
            	} 
            	if($this->data['is_can_read']){
            		$payment_url = "<a href='".base_url()."user_orders/payment/".$data->id."' class='btn btn-info btn-sm white'> Pembayaran</a>";
            	}  

        		if($data->is_active == 0){
        			$status ="<span class='label label-danger'>Belum ada pembayaran</span>";
        		}else{
        			$status ="<span class='label label-success'>Lunas</span>";
        		}
            	

                $nestedData['id'] = $start+$key+1;
                $nestedData['name'] = $data->first_name . ' ' . $data->last_name;
                $nestedData['package_name'] = $data->package_name;
                $nestedData['price'] = 'Rp. '.number_format($data->price); 
                $nestedData['status'] = $status;
           		$nestedData['action'] = $edit_url." ".$payment_url." ".$konfirmasi_url." ".$delete_url;   
                $new_data[] = $nestedData; 
            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $new_data   
                    );
            
        echo json_encode($json_data); 
	}

	public function destroy(){
		$response_data = array();
        $response_data['status'] = false;
        $response_data['msg'] = "";
        $response_data['data'] = array();   

		$id =$this->uri->segment(3);
		$is_deleted = $this->uri->segment(4);
 		if(!empty($id)){
 			$this->load->model("user_orders_model");
			$data = array(
				'is_deleted' => ($is_deleted == 1)?0:1
			); 
			$update = $this->user_orders_model->update($data,array("user_orders.id"=>$id));

        	$response_data['data'] = $data; 
         	$response_data['status'] = true;
 		}else{
 		 	$response_data['msg'] = "ID Harus Diisi";
 		}
		
        echo json_encode($response_data); 
	}

	public function dataList_payment()
	{
		$columns = array( 
            0 =>'id',  
      		1 =>'owner', 
            2 =>'cash',
            3 =>'created',
            4 => 'status'
        ); 
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
  		$search = array();
  		$limit = 0;
  		$start = 0;
        $totalData = $this->payment_model->getCountAllBy($limit,$start,$search,$order,$dir); 

        $searchColumn = $this->input->post('columns');
        $isSearchColumn = false;
        
        if(!empty($searchColumn[3]['search']['value'])){
        	$value = $searchColumn[3]['search']['value'];
        	$isSearchColumn = true;
         	$search['users.first_name'] = $value;
        }  

      	if(!empty($searchColumn[4]['search']['value'])){
        	$value = $searchColumn[4]['search']['value'];
        	$isSearchColumn = true;
         	$search['packages.name'] = $value;
		}
		
		if(!empty($searchColumn[5]['search']['value'])){
			$search_value = $searchColumn[5]['search']['value'];
			$isSearchColumn = true;
			$search = array( 
				"user_orders.status"=>$search_value
			); 
		}

    	if($isSearchColumn){
			$totalFiltered = $this->payment_model->getCountAllBy($limit,$start,$search,$order,$dir); 
        }else{
        	$totalFiltered = $totalData;
        } 
       
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
		$datas = $this->payment_model->getAllBy($limit,$start,$search,$order,$dir);
     	
        $new_data = array();
        if(!empty($datas))
        { 
            foreach ($datas as $key=>$data)
            {  

            	$edit_url = "";
            	$konfirmasi_url = "";
            	$payment_url = "";
     			$delete_url = ""; 

        		if($data->status == 0){
        			$status ="<span class='label label-danger'>Belum lunas</span>";
        		}else{
        			$status ="<span class='label label-success'>Lunas</span>";
        		}
            	

                $nestedData['id'] = $start+$key+1;
                $nestedData['owner'] = $data->owner;
                $nestedData['cash'] = $data->cash;
                $nestedData['created'] = $data->created; 
                $nestedData['status'] = $status;
           		//$nestedData['action'] = $edit_url." ".$payment_url." ".$konfirmasi_url." ".$delete_url;   
                $new_data[] = $nestedData; 
            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered),  
                    "data"            => $new_data   
                    );
            
        echo json_encode($json_data); 
	}

	public function payment($id)
	{ 
		$this->form_validation->set_rules('owner',"Owner", 'trim|required');  
		$this->form_validation->set_rules('cash',"Cash", 'trim|required');  
		$this->form_validation->set_rules('created',"Tanggal Pembayaran", 'trim|required');    
		   
		if ($this->form_validation->run() === TRUE)
		{
			$filename = "image";
			$getData = $this->user_orders_model->getOneBy(array("user_orders.id"=>$id)); 
			if (!empty($_FILES["image"]["name"])) {
			   	$image = $this->_uploadImage($id, $filename);
			} else {
			    $image = $getData->image;
			}
			$data = array(
				'bill_id' 		=> $id,
				'owner' 		=> $this->input->post('owner'),
				'cash' 	 		=> $this->input->post('cash'),
				'created' 		=> $this->input->post('created'),
				'status' 		=> 0,
				'is_deleted' 	=> 0,
				'image'			=> $image,
			);
		    
			$insert = $this->payment_model->insert($data);
			if ($insert)
			{ 
				$this->session->set_flashdata('message', "Pembayaran Berhasil Disimpan");
				redirect("user_orders/payment/".$id,"refresh");
			}else{
				$this->session->set_flashdata('message_error', "Pembayaran Gagal Disimpan");
				redirect("user_orders/payment/".$id,"refresh");
			}
		} 
		else
		{
			if(!empty($_POST)){ 

				$this->session->set_flashdata('message_error',validation_errors());
				return redirect("user_orders/payment/".$id);	

			}else{
				$this->data['content']	= 'admin/user_orders/payment_v'; 
				$this->load->view('admin/layouts/page',$this->data); 
			}  
		}    
		
	}

	public function konfirmasi(){
		$response_data = array();
        $response_data['status'] = false;
        $response_data['msg'] = "";
        $response_data['data'] = array();   

		$id =$this->uri->segment(3);
		$is_active = $this->uri->segment(4);
 		if(!empty($id)){
 			$this->load->model("billing_model");
			$data = array(
				'is_active' => ($is_active == 1)?0:1
			); 
			$update = $this->billing_model->update($data,array("billing.id"=>$id));

        	$response_data['data'] = $data; 
         	$response_data['status'] = true;
 		}else{
 		 	$response_data['msg'] = "ID Harus Diisi";
 		}
		
        echo json_encode($response_data); 
	}

	private function _uploadImage($order_id, $filename)
	{
		$dir = './assets/upload/payment/'.$order_id;
        if (!is_dir($dir)) {  
            mkdir($dir, 0777, true);  
        }

        $config['upload_path']          = $dir;
	    $config['allowed_types']        = 'gif|jpg|png';
	    $config['file_name']            = "PAYMENT_".time().".PNG";
	    $config['overwrite']			= true;
	    //$config['max_size']             = 1024; // 1MB
	    // $config['max_width']            = 1024;
	    // $config['max_height']           = 768;

	    $this->load->library('upload', $config);

	    if ($this->upload->do_upload($filename)) {
	        return $this->upload->data("file_name");
	    }
	}
}
