<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('slider_model');
        //$this->load->model('article_model');
    }	

	public function index() {

		//$config = $this->config_model->list_config();
		
		$data = array(	'title'		=> 'Home Page - Juragan Web',
						'content'   => 'front/home/content'
					);
		$this->load->view('front/layouts/wrapper',$data);
	}
}