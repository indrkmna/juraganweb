<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'core/Admin_Controller.php';
class Packages extends Admin_Controller {
 	public function __construct()
	{
		parent::__construct();
		$this->load->model('packages_model');
		$this->load->model('ion_auth_model');
	}
	public function index()
	{
		$this->load->helper('url');
		if($this->data['is_can_read']){ 
			$this->data['content'] = 'admin/packages/list_v'; 	
		}else{
			$this->data['content'] = 'errors/html/restrict'; 
		}
		
		$this->load->view('admin/layouts/page',$this->data);  
	}


	public function create()
	{ 
		$this->form_validation->set_rules('name',"Nama", 'trim|required');  
		$this->form_validation->set_rules('price',"Harga", 'trim|required'); 
		if ($this->form_validation->run() === TRUE)
		{
			$data = array(
				'name' 			=> $this->input->post('name'),
				'price' 		=> $this->input->post('price'),
				'description' 	=> $this->input->post('description'),
				'is_deleted' 	=> 0
			); 
			$insert = $this->packages_model->insert($data);
			if ($insert)
			{ 
				$this->session->set_flashdata('message', "Paket Berhasil Disimpan");
				redirect("packages");
			}
			else
			{
				$this->session->set_flashdata('message_error',"Paket Gagal Disimpan");
				redirect("packages");
			}
		}else{   
			$this->data['content'] = 'admin/packages/create_v'; 
			$this->load->view('admin/layouts/page',$this->data); 
		}
	} 

	public function edit($id)
	{ 
		$this->form_validation->set_rules('name',"Nama", 'trim|required');  
		$this->form_validation->set_rules('price',"Harga", 'trim|required');
		   
		if ($this->form_validation->run() === TRUE)
		{
			$data = array(
				'name' 			=> $this->input->post('name'),
				'price' 		=> $this->input->post('price'),
				'description' 	=> $this->input->post('description')
			); 
			$update = $this->packages_model->update($data, array("id"=>$id));
			if ($update)
			{ 
				$this->session->set_flashdata('message', "Paket Berhasil Diubah");
				redirect("packages","refresh");
			}else{
				$this->session->set_flashdata('message_error', "Paket Gagal Diubah");
				redirect("packages","refresh");
			}
		} 
		else
		{
			if(!empty($_POST)){ 

				$id = $this->input->post('id'); 
				$this->session->set_flashdata('message_error',validation_errors());
				return redirect("packages/edit/".$id);	

			}else{

				$this->data['id']= $id;
				$data = $this->packages_model->getOneBy(array("id"=>$this->data['id'])); 

		 		$this->data['name'] 		=   (!empty($data))?$data->name:"";
				$this->data['price'] 		=   (!empty($data))?$data->price:"";
				$this->data['description'] 	=   (!empty($data))?$data->description:"";

				$this->data['content'] = 'admin/packages/edit_v'; 
				$this->load->view('admin/layouts/page',$this->data); 
			}  
		}    
		
	} 

	public function dataList()
	{
		$columns = array( 
            0 =>'id',  
      		1 =>'name', 
            2 =>'price',
            3 =>'description',
            4 => 'action'
        ); 
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
  		$search = array();
  		$limit = 0;
  		$start = 0;
        $totalData = $this->packages_model->getCountAllBy($limit,$start,$search,$order,$dir); 

        $searchColumn = $this->input->post('columns');
        $isSearchColumn = false;
        
        if(!empty($searchColumn[3]['search']['value'])){
        	$value = $searchColumn[3]['search']['value'];
        	$isSearchColumn = true;
         	$search['users.first_name'] = $value;
        }  

      	if(!empty($searchColumn[4]['search']['value'])){
        	$value = $searchColumn[4]['search']['value'];
        	$isSearchColumn = true;
         	$search['users.phone'] = $value;
		}
		
		if(!empty($searchColumn[5]['search']['value'])){
			$search_value = $searchColumn[5]['search']['value'];
			$isSearchColumn = true;
			$search = array( 
				"users.email"=>$search_value
			); 
		}

    	if($isSearchColumn){
			$totalFiltered = $this->packages_model->getCountAllBy($limit,$start,$search,$order,$dir); 
        }else{
        	$totalFiltered = $totalData;
        } 
       
        $limit = $this->input->post('length');
        $start = $this->input->post('start');
		$datas = $this->packages_model->getAllBy($limit,$start,$search,$order,$dir);
     	
        $new_data = array();
        if(!empty($datas))
        { 
            foreach ($datas as $key=>$data)
            {  

            	$edit_url = "";
     			$delete_url = "";
     		
            	if($this->data['is_can_edit'] && $data->is_deleted == 0){
            		$edit_url = "<a href='".base_url()."packages/edit/".$data->id."' class='btn btn-primary btn-sm white'><i class='fa fa-pencil'></i> Ubah</a>";
            	}  
            	if($this->data['is_can_delete']){
	            	if($data->is_deleted == 0){
	        			$delete_url = "<a href='#' 
	        				url='".base_url()."packages/destroy/".$data->id."/".$data->is_deleted."'
	        				class='btn btn-danger btn-sm white delete' >NonAktifkan
	        				</a>";
	        		}else{
	        			$delete_url = "<a href='#' 
	        				url='".base_url()."packages/destroy/".$data->id."/".$data->is_deleted."'
	        				class='btn btn-danger btn-sm white delete' 
	        				 >Aktifkan
	        				</a>";
	        		}  
        		}
            	

                $nestedData['id'] = $start+$key+1;
                $nestedData['name'] = $data->name;
                $nestedData['price'] = 'Rp. '.number_format($data->price);
                $nestedData['description'] = $data->description;
           		$nestedData['action'] = $edit_url." ".$delete_url;   
                $new_data[] = $nestedData; 
            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $new_data   
                    );
            
        echo json_encode($json_data); 
	}

	public function destroy(){
		$response_data = array();
        $response_data['status'] = false;
        $response_data['msg'] = "";
        $response_data['data'] = array();   

		$id =$this->uri->segment(3);
		$is_deleted = $this->uri->segment(4);
 		if(!empty($id)){
 			$this->load->model("packages_model");
			$data = array(
				'is_deleted' => ($is_deleted == 1)?0:1
			); 
			$update = $this->packages_model->update($data,array("id"=>$id));

        	$response_data['data'] = $data; 
         	$response_data['status'] = true;
 		}else{
 		 	$response_data['msg'] = "ID Harus Diisi";
 		}
		
        echo json_encode($response_data); 
	}
}
