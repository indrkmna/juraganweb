<?php
/**
 * @author   Natan Felles <natanfelles@gmail.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_create_table_api_limits
 *
 * @property CI_DB_forge         $dbforge
 * @property CI_DB_query_builder $db
 */
class Migration_create_table_user_orders extends CI_Migration {


	public function up()
	{ 
		$table = "user_orders";
		$fields = array(
			'id'           => [
				'type'           => 'INT(11)',
				'auto_increment' => TRUE,
				'unsigned'       => TRUE,
			],
			'user_id'          => [
				'type' => 'INT(11)',
				'null'        => TRUE,
			],
			'package_id'          => [
				'type' => 'INT(11)',
				'null'        => TRUE,
			],
			'payment_name'          => [
				'type' => 'VARCHAR(100)',
				'null'        => TRUE,
			],
			'payment_rek'          => [
				'type' => 'VARCHAR(100)',
				'null'        => TRUE,
			],
			'payment_total'          => [
				'type' => 'INT(11)',
				'null'        => TRUE,
			],
			'payment_date'          => [
				'type' => 'VARCHAR(100)',
				'null'        => TRUE,
			],
			'payment_date'          => [
				'type' => 'VARCHAR(100)',
				'null'        => TRUE,
			],
			'payment_screenshoot' => [
				'type'            => 'TEXT', 
				'null'        => TRUE,
			],
			'status' => [
				'type'            => 'TINYINT',
				'constraint'      => '2',
				'null'        => TRUE,
			],
			'created_at' => [
				'type'            => 'DATETIME',
				'null'        => TRUE,
			],
			'updated_at' => [
				'type'            => 'DATETIME',
				'null'        => TRUE,
			],
			'is_deleted' => [
				'type' => 'TINYINT(4)',
			],

		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table($table);
	 
	}


	public function down()
	{
		$table = "user_orders";
		if ($this->db->table_exists($table))
		{
			$this->db->query(drop_foreign_key($table, 'api_key'));
			$this->dbforge->drop_table($table);
		}
	}

}