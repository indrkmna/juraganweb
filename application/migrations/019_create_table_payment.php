<?php
/**
 * @author   Natan Felles <natanfelles@gmail.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_create_table_api_limits
 *
 * @property CI_DB_forge         $dbforge
 * @property CI_DB_query_builder $db
 */
class Migration_create_table_payment extends CI_Migration {


	public function up()
	{ 
		$table = "payment";
		$fields = array(
			'id'           => [
				'type'           => 'INT(11)',
				'auto_increment' => TRUE,
				'unsigned'       => TRUE,
			],
			'bill_id'          => [
				'type' => 'INT(11)',
			],
			'owner'      => [
				'type' => 'VARCHAR(225)',
			],
			'cash'      => [
				'type' => 'VARCHAR(225)',
			],	
			'image'      => [
				'type' => 'VARCHAR(225)',
			],
			'created'      => [
				'type' => 'DATETIME',
			],		
			'updated'      => [
				'type' => 'DATETIME',
			],	
			'status'          => [
				'type' => 'INT(11)',
			],			
			'is_deleted' => [
				'type' => 'INT(11)',
			],											

		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table($table);
	 
	}


	public function down()
	{
		$table = "payment";
		if ($this->db->table_exists($table))
		{
			$this->db->query(drop_foreign_key($table, 'api_key'));
			$this->dbforge->drop_table($table);
		}		

	}

}