define([
    "jQuery",
    "jqvalidate",
    "bootstrap", 
    "bootstrapDatepicker",
    "sidebar",
    "datatables",
    "datatablesBootstrap",
    "select2",
    ], function (
    $,
    jqvalidate,
    bootstrap,
    bootstrapDatepicker,
    sidebar,
    datatables,
    datatablesBootstrap,
    select2,
    ){ 
    return {
        table:null,
        init: function () {  
            App.initFunc();
            App.initEvent();
            App.initConfirm();
            App.searchTable();
            App.resetSearch();
            $('.select2').select2();
            $(".loading").hide();
        },
        initEvent : function(){
            App.table = $('#table').DataTable({
                "language": {
                    "search": "Cari",
                    "lengthMenu": "Tampilkan _MENU_ baris per halaman",
                    "zeroRecords": "Data tidak ditemukan",
                    "info": "Menampilkan _PAGE_ dari _PAGES_",
                    "infoEmpty": "Tidak ada data yang ditampilkan ",
                    "infoFiltered": "(pencarian dari _MAX_ total records)",
                    "paginate": {
                        "first":      "Pertama",
                        "last":       "Terakhir",
                        "next":       "Selanjutnya",
                        "previous":   "Sebelum"
                    },
                },
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": App.baseUrl+"user/dataList",
                    "dataType": "json",
                    "type": "POST",
                    
                },
               "columns": [
                    { "data": "id" },  
                    { "data": "role_name" }, 
                    { "data": "name" },
                    { "data": "phone" },
                    { "data": "email" },
                    { "data": "photo" },
                    { "data": "action" ,"orderable": false}
                ]      
            }); 

            if($("#form").length > 0){
                $("#form").validate({
                    rules: {
                        first_name: {
                            required: true,
                        },
                        email: {
                            required: true,
                        },
                        phone: {
                            required: true,
                        },
                        role_id: {
                            required: true,
                        },
                        password: {
                            required: ($("#id").val() === "")?true:false,
                        },
                        password_confirm: { 
                            required: ($("#id").val() === "")?true:false,
                            equalTo: '#password',
                        }
                    },
                    messages: {
                        first_name: {
                            required: 'Nama Lengkap Harus Diisi',
                        },
                        email: {
                            required: 'Email Harus Diisi',
                        },
                        phone: {
                            required: 'No Ponsel Harus Diisi',
                        },
                        role_id: {
                            required: 'Jabatan Harus Diisi',
                        },
                        password: {
                            required: "Passowrd Harus Diisi",
                        },
                        password_confirm: {
                            required: "Password Pencocokan Harus Diisi",
                            equalTo: "Password Tidak Sama",
                        }
                    }
                });
            }
        }, 

        searchTable:function(){ 
            $('#search').click(function(event) {

                prop        = $("#kdprop").val();
                kab         = $("#kdkab").val();
                kec         = $("#kdkec").val();
                desa        = $("#getKddesa").val();
                jabatan     = $("#jabatan").val();
                first_name  = $("#first_name").val();
                phone       = $("#phone").val();

                App.table.column(1).search(prop,true,true);
                App.table.column(2).search(kab,true,true);
                App.table.column(3).search(kec,true,true);
                App.table.column(4).search(desa,true,true);
                App.table.column(5).search(jabatan,true,true);
                App.table.column(6).search(first_name,true,true);
                App.table.column(7).search(phone,true,true);
                App.table.draw();
            });

            $('#search_prelist').click(function(event) {
                desa = $("#getKddesa").val();
                kec = $("#kdkec").val();
                kab = $("#kdkab").val();
                prop = $("#kdprop").val();
                status = $("#status").val();
                App.tables.column(5).search(status,true,true);
                App.tables.column(4).search(desa,true,true);
                App.tables.column(3).search(kec,true,true);
                App.tables.column(2).search(kab,true,true);
                App.tables.column(1).search(prop,true,true);
                App.tables.draw();
            });
        },

        resetSearch:function(){
            $('#reset').on( 'click', function () {
                $("#jabatan").val("");
                $("#first_name").val("");
                $("#phone").val("");
                $("#kdprop").val("");
                $("#kdkab").val("");
                $("#kdkec").val("");
                $("#kddesa").val("");

                App.table.search( '' ).columns().search( '' ).draw();
            });

            $('#reset_prelist').on( 'click', function () {
                $("#kdprop").val("");
                $("#kdkab").val("");
                $("#kdkec").val("");
                $("#kddesa").val("");

                App.table.search( '' ).columns().search( '' ).draw();
            });
        },

        initConfirm :function(){
            $('#table tbody').on( 'click', '.delete', function () {
                var url = $(this).attr("url");
                App.confirm("Apakah Anda Yakin Untuk Mengubah Ini?",function(){
                   $.ajax({
                      method: "GET",
                      url: url
                    }).done(function( msg ) {
                        App.table.ajax.reload(null,true);
                    });        
                })
            });
        }
    }
});